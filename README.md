﻿# Custom ASP.NET Action Filter

A custom ASP.NET Action Filter can be a very useful tool when you want to add functionality to Web API actions without having to extend controller classes or copy paste code into each action. ASP.NET provides the `IActionFilter` interface that allows you to create classes that implement the functionality. At startup in, `Startup` (ha), you can modify the `MvcOptions` which is where you can add your custom Action Filter. A couple uses for an Action Filter, but not limited to, authentication and logging.

I used .NET Core 2.2 for this project.

## Prerequisites

- .NET Core 2.2

## Create an Action Filter

Create a directory in the project named `ActionFilters`.

In the new `ActionFilters` directory, create a new class called `LoggingActionFilter` that extends `IActionFilter`.

```csharp

// ...
using Microsoft.AspNetCore.Mvc.Filters;

public class LoggingActionFilter : IActionFilter
{
    public void OnActionExecuted(ActionExecutedContext context)
    {
        throw new NotImplementedException();
    }

    public void OnActionExecuting(ActionExecutingContext context)
    {
        throw new NotImplementedException();
    }
}

```

You will be required to implement the methods `OnActionExecuted` and `OnActionExecuting`. Before we can do that, we must have a logger.

```csharp

// ...
using Microsoft.Extensions.Logging;

public class LoggingActionFilter : IActionFilter
{
    private readonly ILogger<LoggingActionFilter> _logger;

    public LoggingActionFilter(ILogger<LoggingActionFilter> logger)
    {
        _logger = logger;
    }

    // ...
}

```

We are going to have ASP.NET's depedency injection give us an `ILogger` instance and assign it to a field. Now we can implement `OnActionExecuted` and `OnActionExecuting`.

```csharp

public class LoggingActionFilter : IActionFilter
{
    // ...

    public void OnActionExecuted(ActionExecutedContext context)
    {
        _logger.LogInformation("Action finished executing.");
    }

    public void OnActionExecuting(ActionExecutingContext context)
    {
        _logger.LogInformation("Action started executing.");
    }
}

```

In order for the logging to work, we need to configure our logging in the `Program` class.

```csharp

public class Program
{
    public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
        WebHost.CreateDefaultBuilder(args)
            .ConfigureLogging((hostingContext, logging) =>
            {
                logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                logging.AddConsole();
                logging.AddDebug();
                logging.AddEventSourceLogger();
            })
            .UseStartup<Startup>();
}

```

All we are doing is logging to the console by adding the `ConfigureLogging` section of code to the `Program.CreateWebHostBuilder` method. And we'll change our `appsettings.Development.json`.

```json

{
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "System": "Information",
      "Microsoft": "Information"
    }
  }
}

```

Finally, we need to register our Action Filter in `Startup`. We can do this by using the `IServiceCollection.AddMvc` method overload that expects an `Action<MvcOptions>` parameter.

```csharp

// ...
using CustomAspDotNetActionFilter.ActionFilters;

public class Startup
{
    // ...

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddMvc(options =>
        {
            options.Filters.Add<LoggingActionFilter>();
        })
            .SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
    }
}

```

## Run it!

When we run the ASP.NET app in Debug, we should see our `http://localhost:[port]/api/values` URL load. In the Debug Output, we should also see somewhere nested in all the stuff the following:

```sh

...

CustomAspDotNetActionFilter.ActionFilters.LoggingActionFilter:Information: Action started executing.

...

CustomAspDotNetActionFilter.ActionFilters.LoggingActionFilter:Information: Action finished executing.

...

```

## Bonus: Log the action executing/executed

We can use the context to find out the meta data of the action being called.

```csharp

public class ActionMetaData
{
    public string ControllerName { get; set; }
    public string MethodName { get; set; }
}

public class LoggingActionFilter : IActionFilter
{
    // ...

    public void OnActionExecuted(ActionExecutedContext context)
    {
        var metaData = ExtractActionMetaData(context);
        _logger.LogInformation($"Action {metaData.MethodName} in {metaData.ControllerName} finished executing.");
    }

    public void OnActionExecuting(ActionExecutingContext context)
    {
        var metaData = ExtractActionMetaData(context);
        _logger.LogInformation($"Action {metaData.MethodName} in {metaData.ControllerName} started executing.");
    }

    private ActionMetaData ExtractActionMetaData(ActionContext context)
    {
        var methodInfo = (context.ActionDescriptor as ControllerActionDescriptor).MethodInfo;
        return new ActionMetaData
        {
            ControllerName = methodInfo.DeclaringType.Name,
            MethodName = methodInfo.Name,
        };
    }
}

```

The above code accesses the `MethodInfo` property provided by the context to get additional information about the execution.

```sh

...

CustomAspDotNetActionFilter.ActionFilters.LoggingActionFilter:Information: Action Get in ValuesController started executing.

...

CustomAspDotNetActionFilter.ActionFilters.LoggingActionFilter:Information: Action Get in ValuesController finished executing.

...

```

## Final notes

Logging like this is redundant to what ASP.NET already provides for tracing HTTP requests to Actions. What might be useful in a custom Action Filter may be related to custom Attributes you may create for your Actions. Your custom Attributes may control access or indicate to log more information for analytics purposes. In that case, you can use the following line of code in your `OnActionExecuted` or `OnActionExecuting` methods:

`var myCustomActionAttributes = methodInfo.GetCustomAttributes<MyCustomAttribute>(true);`

Which will give you an array of Attributes applied to the Action of the type or inheriting `MyCustomAttribute`.

It is nice to know there are several layers of composition are available for our disposal in ASP.NET Core.