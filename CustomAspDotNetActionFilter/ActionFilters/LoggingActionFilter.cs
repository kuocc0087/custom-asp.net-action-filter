﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;

namespace CustomAspDotNetActionFilter.ActionFilters
{
    public class ActionMetaData
    {
        public string ControllerName { get; set; }
        public string MethodName { get; set; }
    }

    public class LoggingActionFilter : IActionFilter
    {
        private readonly ILogger<LoggingActionFilter> _logger;

        public LoggingActionFilter(ILogger<LoggingActionFilter> logger)
        {
            _logger = logger;
        }

        public void OnActionExecuted(ActionExecutedContext context)
        {
            var metaData = ExtractActionMetaData(context);
            _logger.LogInformation($"Action {metaData.MethodName} in {metaData.ControllerName} finished executing.");
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var metaData = ExtractActionMetaData(context);
            _logger.LogInformation($"Action {metaData.MethodName} in {metaData.ControllerName} started executing.");
        }

        private ActionMetaData ExtractActionMetaData(ActionContext context)
        {
            var methodInfo = (context.ActionDescriptor as ControllerActionDescriptor).MethodInfo;
            return new ActionMetaData
            {
                ControllerName = methodInfo.DeclaringType.Name,
                MethodName = methodInfo.Name,
            };
        }
    }
}